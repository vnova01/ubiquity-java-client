

# Operation

## oneOf schemas
* [MultiTransferOperation](MultiTransferOperation.md)
* [TransferOperation](TransferOperation.md)

## Example
```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.model.Operation;
import com.gitlab.blockdaemon.ubiquity.model.MultiTransferOperation;
import com.gitlab.blockdaemon.ubiquity.model.TransferOperation;

public class Example {
    public static void main(String[] args) {
        Operation exampleOperation = new Operation();

        // create a new MultiTransferOperation
        MultiTransferOperation exampleMultiTransferOperation = new MultiTransferOperation();
        // set Operation to MultiTransferOperation
        exampleOperation.setActualInstance(exampleMultiTransferOperation);
        // to get back the MultiTransferOperation set earlier
        MultiTransferOperation testMultiTransferOperation = (MultiTransferOperation) exampleOperation.getActualInstance();

        // create a new TransferOperation
        TransferOperation exampleTransferOperation = new TransferOperation();
        // set Operation to TransferOperation
        exampleOperation.setActualInstance(exampleTransferOperation);
        // to get back the TransferOperation set earlier
        TransferOperation testTransferOperation = (TransferOperation) exampleOperation.getActualInstance();
    }
}
```


