

# Asset


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tokenId** | **Long** |  |  [optional]
**imageUrl** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**contract** | [**Contract**](Contract.md) |  |  [optional]
**wallets** | [**List&lt;AssetWallet&gt;**](AssetWallet.md) |  |  [optional]
**attributes** | [**List&lt;AssetTrait&gt;**](AssetTrait.md) |  |  [optional]
**mintDate** | **Long** |  |  [optional]



