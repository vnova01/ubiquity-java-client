package com.gitlab.blockdaemon.ubiquity.tx.btc;


public class UnspentOutputInfo {
	public byte[] txHash;
	public Script scriptPubKey;
	public long value;
	public int outputIndex;
	public KeyPair keys;

	public UnspentOutputInfo( ) {

	}

	public UnspentOutputInfo(KeyPair keys, byte[] txHash, Script scriptPubKey, long value,
			int outputIndex) {
		this.keys = keys;
		this.txHash = txHash;
		this.scriptPubKey = scriptPubKey;
		this.value = value;
		this.outputIndex = outputIndex;
	}

	public UnspentOutputInfo txHash(byte[] txHash) {
		this.txHash = txHash;
		return this;
	}

	public UnspentOutputInfo txHash(String txHash) {
		this.txHash = BtcService.fromHex(txHash);
		return this;
	}

	public UnspentOutputInfo scriptOfUnspentOutput(Script scriptPubKey) {
		this.scriptPubKey = scriptPubKey;
		return this;
	}

	public UnspentOutputInfo value(long value) {
		this.value = value;
		return this;
	}

	public UnspentOutputInfo outputIndex(int outputIndex) {
		this.outputIndex = outputIndex;
		return this;
	}

	public UnspentOutputInfo keys(KeyPair keys) {
		this.keys = keys;
		return this;
	}

	public UnspentOutputInfo build() {
		if (this.scriptPubKey == null) {
			this.scriptPubKey = Script.buildOutput(this.keys.address.toString());
		}
		return this;
	}
}
